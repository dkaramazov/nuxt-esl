const { Students } = require('../api/airtable').bases
const { Student } = require('../api/airtable').models

const get = async function(params) {
  const data = await Students.select(params).all()
  return data.map(function(d) {
    return Student.transform(d)
  })
}

const getById = async function(id) {
  try {
    let record = await Students.find(id)
    return Student.transform(record)
  } catch (err) {
    console.log(err)
    return null
  }
}

const create = async function(s) {
  try {
    const record = await Student.create(Student.create(s))
    return Student.map(record)
  } catch (err) {
    console.log(err)
    return
  }
}

const update = async function(id, params) {
  try {
    const record = await Students.update(id, params)
    return Student.map(record)
  } catch (err) {
    console.log(err)
    return
  }
}

const toggle = async function(id, fieldName) {
  const student = await getById(id)
  var obj = {}
  obj[fieldName] = !student[fieldName]
  const record = await Students.update(id, obj)
  return Student.map(record)
}

module.exports = {
  create,
  getById,
  get,
  toggle,
  update
}
