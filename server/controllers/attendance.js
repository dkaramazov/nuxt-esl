const { Attendance } = require('../api/airtable').bases
const { AttendanceRecord } = require('../api/airtable').models

const get = async function(params) {
  const data = await Attendance.select(params).all()
  return data.map(function(d) {
    return AttendanceRecord.transform(d)
  })
}

const getById = async function(id) {
  try {
    let record = await Attendance.find(id)
    return AttendanceRecord.transform(record)
  } catch (err) {
    console.log(err)
    return null
  }
}

const create = async function(s) {
  try {
    const record = await Attendance.create(AttendanceRecord.create(s))
    return AttendanceRecord.transform(record)
  } catch (err) {
    console.log(err)
    return
  }
}

const upsert = async function(attendance) {
  try {
    if (attendance.id) {
      attendance = await this.update(attendance.id, attendance)
    } else {
      attendance = await this.create(attendance)
    }
    return attendance
  } catch (err) {
    console.log(err)
    return
  }
}

const update = async function(id, attendance) {
  try {
    return AttendanceRecord.transform(await Attendance.update(id, AttendanceRecord.create(attendance)))
  } catch (err) {
    console.log(err)
    return
  }
}

const toggle = async function(id, fieldName) {
  const student = await getById(id)
  var obj = {}
  obj[fieldName] = !student[fieldName]
  const record = await Attendance.update(id, obj)
  return AttendanceRecord.transform(record)
}

module.exports = {
  create,
  getById,
  get,
  toggle,
  update,
  upsert
}
