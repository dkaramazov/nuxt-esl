const { Classes } = require('../api/airtable').bases
const { Class } = require('../api/airtable').models

const get = async function(params) {
  const data = await Classes.select(params).all()
  return data.map(function(d) {
    return Class.transform(d)
  })
}

const getById = async function(id) {
  try {
    let record = await Classes.find(id)
    return Class.transform(record)
  } catch (err) {
    console.log(err)
    return null
  }
}

const create = async function(s) {
  try {
    const record = await Class.create(Class.create(s))
    return Class.map(record)
  } catch (err) {
    console.log(err)
    return
  }
}

const update = async function(id, params) {
  try {
    const record = await Classes.update(id, params)
    return Class.map(record)
  } catch (err) {
    console.log(err)
    return
  }
}

const toggle = async function(id, fieldName) {
  const student = await getById(id)
  var obj = {}
  obj[fieldName] = !student[fieldName]
  const record = await Classes.update(id, obj)
  return Class.map(record)
}

module.exports = {
  create,
  getById,
  get,
  toggle,
  update
}
