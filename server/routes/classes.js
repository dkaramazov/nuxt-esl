const express = require('express')
const router = express.Router()
const classController = require('../controllers/class')

router.get('/', async (req, res) => {
  try{
    var classes = await classController.get({})
    res.send({ classes })
  } catch(err){
    console.log(err)
    res.send({err})
    return
  }
})

router.get('/:id', async (req, res) => {
  try{
    var classes = await classController.getById(req.params.id)
    res.send({ classes })
  } catch(err){
    console.log(err)
    res.send({err})
    return
  }
})

module.exports = router
