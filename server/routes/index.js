const students = require('./students');
const attendance = require('./attendance');
const classes = require('./classes');

module.exports = {
  students,
  classes,
  attendance
}
