const express = require('express')
const router = express.Router()
const attendanceController = require('../controllers/attendance')

router.get('/', async (req, res) => {
  try {
    var records = await attendanceController.get({})
    res.send({ records })
  } catch (err) {
    console.log(err)
    res.send({ err })
    return
  }
})

router.get('/:date', async (req, res) => {
  try {
    var records = await attendanceController.get({ Date: req.params.date })
    res.send({ records })
  } catch (err) {
    console.log(err)
    res.send({ err })
    return
  }
})

router.post('/upsert', async (req, res) => {
  try {
    let attendance = await attendanceController.upsert(req.body)
    res.send({ attendance })
  } catch (err) {
    console.log(err)
    return
  }
})

router.post('/add', async (req, res) => {
  try {
    var attendance = await attendanceController.create({
      name: req.body.name,
      students: req.body.students,
      date: req.body.date,
      teachers: req.body.teachers || []
    })
    res.send({ attendance })
  } catch (err) {
    console.log(err)
    return
  }
})

module.exports = router
