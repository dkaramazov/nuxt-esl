const express = require('express')
const router = express.Router()
const studentController = require('../controllers/student')

router.get('/', async (req, res) => {
  try {
    var students = await studentController.get({})
    res.send({ students })
  } catch (err) {
    console.log(err)
    res.send({ err })
    return
  }
})

router.get('/:id', async (req, res) => {
  try {
    var student = await studentController.getById(req.params.id)
    res.send({ student })
  } catch (err) {
    console.log(err)
    res.send({ err })
    return
  }
})

router.get('/search/:search', async (req, res) => {
  try {
    var records = await studentController.get({
      view: 'Main View',
      formula: `FIND(${req.params.search}, {Name})`
    })
    res.send({ records })
  } catch (err) {
    console.log(err)
    res.send({ err })
    return
  }
})

module.exports = router
