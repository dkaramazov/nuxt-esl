var Airtable = require('airtable');
const airtable = new Airtable({ apiKey: process.env.AIRTABLE_API_KEY });
const base = airtable.base(process.env.AIRTABLE_BASE_KEY);

const Students = base('Students');
const Attendance = base('Attendance');
const Classes = base('Classes');

const Student = require('./models/student');
const AttendanceRecord = require('./models/attendance');
const Class = require('./models/class');

module.exports = {
    bases: {
        Students,
        Attendance,
        Classes,
    },
    models: {
        Student,
        AttendanceRecord,
        Class,
    }
}
