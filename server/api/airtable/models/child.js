const create = ({ name, parent, age, allergies, allergyDetail }) => {
    return {
        "Name": name,
        "Parent": parent,
        "Age": age,
        "Allergies to Food": allergies,
        "Allergy Detail": allergyDetail,
    }
}

const transform = (record) => {
    return {
        id: record.getId(),
        name: record.get("Name"),
        parent: record.get("Parent"),
        age: record.get("Age"),
        allergies: record.get("Allergies to Food"),
        allergyDetail: record.get("Allergy Detail")
    }
}

module.exports = {
    create,
    transform
}