const create = ({ name, date, students, teachers }) => {
    return {
        "Name": name,
        "Books": books,
        "Students": students,
        "Teachers": teachers,
    }
}

const transform = (record) => {
    return {
        id: record.getId(),
        name: record.get("Name"),
        books: record.get("Books"),
        students: record.get("Students"),
        teachers: record.get("Teachers"),
        createdDate: record.get("CreatedTime")
    }
}

module.exports = {
    create,
    transform
}