const create = ({ name, type, condition, isbn, notes, purchaseDate, purchasePrice, sold, soldPrice, student, teacher }) => {
    return {
        "Name": name,
        "Type": type,
        "Condition": condition,
        "ISBN": isbn,
        "Notes": notes,
        "Purchased": purchaseDate,
        "Purchase Price": purchasePrice,
        "Sold": sold,
        "Sold Price": soldPrice,
        "Student": student,
        "Teacher": teacher,
    }
}

const transform = (record) => {
    return {
        id: record.getId(),
        name: record.get("Name"),
        parent: record.get("Parent"),
        age: record.get("Age"),
        allergies: record.get("Allergies to Food"),
        allergyDetail: record.get("Allergy Detail")
    }
}

module.exports = {
    create,
    transform
}