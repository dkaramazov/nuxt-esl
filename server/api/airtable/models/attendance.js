const create = ({ name, date, students, teachers }) => {
    return {
        "Name": name,
        "Date": date,
        "Students": students,
        "Teachers": teachers,
    }
}

const transform = (record) => {
    return {
        id: record.getId(),
        name: record.get("Name"),
        date: record.get("Date"),
        students: record.get("Students"),
        teachers: record.get("Teachers"),
    }
}

module.exports = {
    create,
    transform
}
