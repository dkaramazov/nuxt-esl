const create = ({ name, section1, section2, section3, level, currentLevel }) => {
    return {
        "Name": name,
        "Section 1: Listening Score": section1,
        "Section 2: Reading": section2,
        "Section 3: Language Use Score": section3,
        "Level": level,
        "Current Level": currentLevel
    }
}

const transform = (record) => {
    return {
        id: record.getId(),
        section1: record.get("Section 1: Listening Score"),
        section2: record.get("Section 2: Reading"),
        section3: record.get("Section 3: Language Use Score"),
        level: record.get("Level"),
        currentLevel: record.get("Current Level"),
        createdDate: record.get("Created Time")
    }
}

module.exports = {
    create,
    transform
}