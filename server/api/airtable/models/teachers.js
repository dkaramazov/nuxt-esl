const create = ({ name, className, attendance, books }) => {
    return {
        "Name": name,
        "Class": className,
        "Attendance": attendance,
        "Books": books,
    }
}

const transform = (record) => {
    return {
        id: record.getId(),
        name: record.get("Name"),
        className: record.get("Class"),
        attendance: record.get("Attendance"),
        books: record.get("Books"),
    }
}

module.exports = {
    create,
    transform
}