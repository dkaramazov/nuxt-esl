const create = ({ name, churched, country, className, active, joinDate, phone, birthday, email, languages, studiedBefore, allergies, allergyDetail, attendance, howDidTheyHearAboutESL, interviews, children, notes, books, placementTests, createdDate }) => {
    return {
        'Name': name,
        'Churched': churched,
        'Country': country,
        'Class': className,
        'Active': active,
        'Join Date': joinDate,
        'Phone': phone,
        'Birthday': birthday,
        'Email Address': email,
        'Languages': languages,
        'Studied ESL Before': studiedBefore,
        'Allergies to Food': allergies,
        'Allergy Detail': allergyDetail,
        'Attendance': attendance,
        'How did they hear about ESL': howDidTheyHearAboutESL,
        'Interviews': interviews,
        'Children': children,
        'Notes': notes,
        'Books': books,
        'Placement Test': placementTests,
        'CreatedTime': createdDate
    }
}

const transform = (record) => {
    return {
        id: record.getId(),
        name: record.get('Name'),
        className: record.get('Class'),
        country: record.get('Country'),
        active: record.get('Active'),
        joinDate: record.get('Join Date'),
        phone: record.get('Phone'),
        birthday: record.get('Birthday'),
        email: record.get('Email Address'),
        languages: record.get('Languages'),
        studiedBefore: record.get('Studied ESL Before'),
        allergies: record.get('Allergies to Food'),
        allergyDetail: record.get('Allergy Detail'),
        attendance: record.get('Attendance'),
        howDidTheyHearAboutESL: record.get('How did they hear about ESL'),
        interviews: record.get('Interviews'),
        children: record.get('Children'),
        notes: record.get('Notes'),
        books: record.get('Books'),
        placementTests: record.get('Placement Test'),
        createdDate: record.get('CreatedTime')
    }
}

module.exports = {
    create,
    transform
}
