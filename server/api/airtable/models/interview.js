const create = ({ date, student, level, notes }) => {
    return {
        "Interview Date": date,
        "Student": student,
        "Level": level,
        "Notes": notes,
    }
}

const transform = (record) => {
    return {
        id: record.getId(),
        date: record.get("Interview Date"),
        student: record.get("Student"),
        level: record.get("Level"),
        notes: record.get("Notes"),
        createdDate: record.get("CreatedTime")
    }
}

module.exports = {
    create,
    transform
}