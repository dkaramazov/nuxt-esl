export const state = () => ({
  attendance: null
})

export const mutations = {
  upsertAttendance(state, attendance) {
    state.attendance = attendance
  }
}

export const actions = {
  addAttendance(vuexContext, attendance) {
    vuexContext.commit('upsertAttendance', attendance)
  }
}
export const getters = {
  getAttendance(state) {
    return state.attendance
  }
}
