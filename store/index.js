export const state = () => ({
  students: [],
  classes: []
})

export const mutations = {
  setStudents(state, students) {
    state.students = students
  },
  upsertStudent(state, student) {
    let s = state.students.find(x => x.id === student.id)
    s = student
  },
  setClasses(state, classes) {
    state.classes = classes
  }
}
export const actions = {
  setStudents(vuexContext, students) {
    vuexContext.commit('setStudents', students)
  },
  setClasses(vuexContext, classes) {
    vuexContext.commit('setClasses', classes)
  }
}
export const getters = {
  getStudents(state) {
    return state.students
  },
  getClasses(state) {
    return state.classes
  }
}
